﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
	public float speed = 10f;


	private Transform target;
	private int currentPathPointIndex = 0;

	void Start()
	{
		target = PathpointManager.points[0];
	}


	void Update()
	{
		Vector3 directionToPoint = target.position - transform.position;
		transform.Translate(directionToPoint.normalized * speed * Time.deltaTime, Space.World);
	
		if (Vector3.Distance(transform.position, target.position) <= 0.4f)
		{
			if(currentPathPointIndex >= PathpointManager.points.Length - 1)
			{
				Destroy(gameObject);
				PlayerLife.currentLife -= 1;
				return;
			}

			currentPathPointIndex++;
			target = PathpointManager.points[currentPathPointIndex];
		}
	}
}
