﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WaveManager : MonoBehaviour
{

	public Transform enemyPrefab;
	public Transform spawnPoint;

	public float timeBetweenWaves = 4f;
	float countdownTimer = 2f;

	int currentWave = 0;

	void Update()
	{
		if (countdownTimer <= 0f)
		{
			StartCoroutine(SpawnWave());
			countdownTimer = timeBetweenWaves;
		}

		countdownTimer -= Time.deltaTime;
	}

	IEnumerator SpawnWave()
	{
		currentWave++;

		for (int i = 0; i < currentWave; i++)
		{
			SpawnEnemy();
			yield return new WaitForSeconds(0.5f);
		}
	}

	void SpawnEnemy()
	{
		Instantiate(enemyPrefab, spawnPoint.position, spawnPoint.rotation);
	}

}