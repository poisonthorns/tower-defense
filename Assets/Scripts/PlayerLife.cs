﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerLife : MonoBehaviour
{

	public static int currentLife = 50;
	public Text playerLifeText;
	public Text playerState;

	void Start()
	{
		UpdateLifeUI();
	}

	void Update()
	{
		if (currentLife <= 0)
		{
			currentLife = 0;
			UpdateLifeUI();

			GameOver();
			return;
		}

		UpdateLifeUI();
	}

	void UpdateLifeUI()
	{
		playerLifeText.text = "" + currentLife;
	}
	
	void GameOver()
	{
		playerState.text = "Game over.\nYou suck!";
		Debug.Log("Game over!");
	}
}
